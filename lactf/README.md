Team [Cr4zyF0x](https://ctftime.org/team/163248)\
213th of 980 teams\
3207 points

This was the second ctf I competed in, this season. It was a joined effort with n4nika, who also wrote a [few writups](https://github.com/n4nika/lactf2023_writeup) for this ctf.

<img src="resources/lactf.png" alt="lactf banner" style="height:200px;"/>