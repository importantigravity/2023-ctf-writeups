# Hidden in Plain Sheets
```
misc/hidden in plain sheets
burturt
251 solves / 315 points

I found this google sheets link on the internet. I'm sure it's hiding something, but I can't find anything? Can you find the flag?

Choose any link (they're all the same): [Link 1](https://docs.google.com/spreadsheets/d/1OYx3lCccLKYgOvzxkRZ5-vAwCn3mOvGUvB4AdnSbcZ4/edit) [Link 2](https://docs.google.com/spreadsheets/d/17A1f0z8rmR7356fcHmHTHt3Y0JMgcHlGoflADtNXeOU/edit) [Link 3](https://docs.google.com/spreadsheets/d/1ULdm_KCOYCWuf6gqpg6tm0t-wnWySX_Bf3yUYOfZ2tw/edit)
```
The link given in the challenge leads to a google sheets project. On first glance this looks pretty boring, but after some digging, I was able to find a protected sheet called flag.

![protectet sheets list](resources/hidden_in_plain_sheets-protected_sheets_list.png)

It is marked view only, which I thought would mean it would be possible to view it, but upon clicking it nothing happend. So we found the sheet, but we are not able to open it - interesting...

After that, I decided to look through the Options I had since they were very limited. There wasn't much I could find except the search feature.
On closer inspection of the search feature I found that you could search all sheets, but even better there was an option to search a specific range. The syntax of this seemed to be
```sheetname!cell```.

Another useful feature is the search using regex, which saved me from a lot of brute forcing. I constructed a regex that would basically be triggered by any character of the flag `[!-}]+` which is not the cleanest solution, but it did the job.

Combining these two observations I was able to get any set of characters in any cell of the protected sheet. So the combination of `Find: [!-}]+` and `Search - Specific Range: flag!A1` resulted in 
`A match was found in hidden cell flag!A1 with value "l"`.
Going forward it was just a matter of looking at cells B1, C1, D1, ...

![the value of the cell AQ1 in the flag sheet being leaked using a regex search](resources/hidden_in_plain_sheets-search_exploitation.png)

And just like this, we get the flag:

lactf{H1dd3n_&_prOt3cT3D_5h33T5_Ar3_n31th3r}
